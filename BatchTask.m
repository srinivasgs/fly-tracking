% BatchTask.m
% created by Srinivas Gorur-Shandilya at 15:49 , 19 February 2014. Contact me at http://srinivas.gs/contact/
% splits up a task into as many folders as there are cores on the machine, or as you want.

function [] = BatchTask(n)

if nargin < 1
	n = feature('numCores');
	disp('Splitting task into as many cores as there are on this machine...')
end
allfiles = dir('*.mat'); % run on all *.mat files
% make sure they are real files
badfiles= [];
for i = 1:length(allfiles)
	if strcmp(allfiles(i).name(1),'.')
		badfiles = [badfiles i];
	end
end
allfiles(badfiles) = [];

% ignore completely analysed files
s = DetermineStateOfMATFiles(allfiles);
allfiles(s==3) = [];


batch_size = ceil(length(allfiles)/n);

for i = 1:n
	thisfolder=(strcat('fv_batch',mat2str(i)));
	mkdir(thisfolder)

	for j = 1:min([batch_size length(allfiles)])
		
		movefile(allfiles(j).name,strcat(thisfolder,oss,allfiles(j).name))

		% also move the AVI file
		vfile = strcat(allfiles(j).name(1:end-4),'.avi');
		movefile(vfile,strcat(thisfolder,oss,vfile));
	end
	% mark it as moved
	allfiles(1:batch_size) = [];

end

disp('All done.')

% save where this is 
temp=mfilename('fullpath');
s=strfind(temp,oss);
temp = temp(1:s(end));
filename = strcat(temp,'batch_task.mat');
data_here = cd;
save(filename,'data_here')



